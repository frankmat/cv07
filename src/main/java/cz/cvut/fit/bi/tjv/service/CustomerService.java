package cz.cvut.fit.bi.tjv.service;


import cz.cvut.fit.bi.tjv.dao.CustomerDao;
import cz.cvut.fit.bi.tjv.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerDao customerDao;

    @Transactional
    public void add(Customer customer) {
        customerDao.persist(customer);
    }

    @Transactional
    public void addAll(Collection<Customer> customers) {
        for (Customer customer : customers) {
            customerDao.persist(customer);
        }
    }

    @Transactional(readOnly = true)
    public List<Customer> listAll() {
        return customerDao.findAll();
    }
}